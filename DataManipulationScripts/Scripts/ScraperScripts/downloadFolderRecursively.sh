#!/bin/bash

#To Call this script use: ./downloadFolderRecursively.sh path-to-save-files

cd $1
mkdir FullScrape
cd FullScrape
exceptions="\"*.html, APTNotesReports_20200208.zip\""
websiteURL="https://mcfp.felk.cvut.cz/publicDatasets/"

wget -r --no-check-certificate -A "*.md, *.pcap, *.binetflow" --reject-regex ".*CTU-13-Extended-Dataset*.|.*CTU-13-Dataset*.|.*IoT*.|.*malware*.|.*Malware*.|.*Botnet*.|.*Attack*.|.*Mixed*.|.*HoneyPot*.|.*Honeypot*.|.*cacic2018*.|.*BAB0*.|.*pocorgtfo*.|.*VirtualBox*." $websiteURL

#wget -r -np -nH --cut-dirs=3 -R $exceptions --no-check-certificate $websiteURL