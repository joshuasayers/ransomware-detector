#!/bin/bash

#https://archive.wrccdc.org/pcaps/
#To Call this script use: ./downloadFolderRecursivelyWRCCDC.sh path-to-save-files

cd $1
mkdir WRCCDCScrape
cd WRCCDCScrape
exceptions="\"*.html\""
websiteURL="https://archive.wrccdc.org/pcaps/"

wget -r --no-check-certificate -A "*.pcap.gz, *.pcap, *.md" --reject-regex ".*2017*.|.*2012*." $websiteURL
