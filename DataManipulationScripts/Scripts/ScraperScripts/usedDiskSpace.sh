#!/bin/bash

# Shell script used to check how much space the Kali disk instance has available.
# Useful for checking space available when webscraping.

df -h /dev/sda2 | grep "/dev/sda2" | awk '{ print $5 }'
