filePathToTestFolder = "TestFiles"


# Test the scrape script and check that its creating the right folder
def testingDownloadFolderRecursively(bash):
    # Run command with a 4 second timout.
    bash.send_raw("timeout 4 ../downloadFolderRecursively.sh " + filePathToTestFolder)
    # Check whether the folder exists - if it does the scrape has started, save output to variable
    output = bash.path_exists("TestFiles/FullScrape")
    # Remove the folder
    bash.send("rm -rf TestFiles/FullScrape")
    # Assert the output of whether the folder exists as the test criteria
    assert output


# Test the scrape script and check that its creating the right folder and folders within
def testingDownloadFolderRecursivelyContents(bash):
    # Run command with a 5 second timout.
    bash.send_raw("timeout 5 ../downloadFolderRecursively.sh " + filePathToTestFolder)
    # Check whether the folder exists - if it does the scrape has started, save output to variable
    output = bash.path_exists("TestFiles/FullScrape/mcfp.felk.cvut.cz/publicDatasets/")
    # Remove the folder
    bash.send("rm -rf TestFiles/FullScrape")
    # Assert the output of whether the folder exists as the test criteria
    assert output