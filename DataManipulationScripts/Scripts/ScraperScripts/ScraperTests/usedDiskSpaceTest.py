import re

#Test that the usedDiskSpace.sh script returns disk space available
def testingUsedDiskSpaceReturnsNumericPercentageOnly(bash):
    output = bash.run_script("../usedDiskSpace.sh")
    result = re.match('^\\d{1,3}%$', output)
    print(result, output)
    assert result