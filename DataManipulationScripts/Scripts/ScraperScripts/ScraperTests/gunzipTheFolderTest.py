import re
filePathToTestFolder = "/home/blackdwarf/Desktop/RansomwareDetectorProject/GitLabRepository/ransomware-detector" \
                       "/DataManipulationScripts/Scripts/ScraperScripts/ScraperTests/TestFiles/TestCompressedFile "

# Test the gunzipTheFolder script by unziping a test folder.
def testingGunzipTheFolderUnzipsTheWholeFolder(bash):
    bash.send("gzip -r TestFiles/TestCompressedFile")
    output = bash.send("../gunzipTheFolder.sh " + filePathToTestFolder)
    result = bash.path_exists("./TestFiles/TestCompressedFile")
    print(result, output)
    assert result


# Test the gunzipTheFolder script by unziping a test folder with Relative Path should be an Error
def testingGunzipTheFolderUnzipsTheWholeFolderRelativePathFail(bash):
    try:
        bash.send("../gunzipTheFolder.sh ScraperTests/TestFiles/TestCompressedFile")
    except:
        #If theres an exception this test passed.
        assert True
    else:
        #If an exception was not raised this test failed.
        assert False