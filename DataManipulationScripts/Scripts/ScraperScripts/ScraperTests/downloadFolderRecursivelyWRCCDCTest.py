filePathToTestFolder = "TestFiles"


# Test the scrape script and check that its creating the right folder
def testingDownloadFolderRecursivelyWRCCDC(bash):
    # Run command with a 4 second timout.
    bash.send_raw("timeout 4 ../downloadFolderRecursivelyWRCCDC.sh " + filePathToTestFolder)
    # Check whether the folder exists - if it does the scrape has started, save output to variable
    output = bash.path_exists("TestFiles/WRCCDCScrape")
    # Remove the folder
    bash.send("rm -rf TestFiles/WRCCDCScrape")
    # Assert the output of whether the folder exists as the test criteria
    assert output


# Test the scrape script and check that its creating the right folder and folders within
def testingDownloadFolderRecursivelyWRCCDCContents(bash):
    # Run command with a 5 second timout.
    bash.send_raw("timeout 5 ../downloadFolderRecursivelyWRCCDC.sh " + filePathToTestFolder)
    # Check whether the folder exists - if it does the scrape has started, save output to variable
    output = bash.path_exists("TestFiles/WRCCDCScrape/archive.wrccdc.org/pcaps/")
    # Remove the folder
    bash.send("rm -rf TestFiles/WRCCDCScrape")
    # Assert the output of whether the folder exists as the test criteria
    assert output