# Convert tshark conversation output into csv
# Mostafa Dahshan <https://github.com/mdahshan>

# References
# https://www.w3resource.com/python-exercises/re/python-re-exercise-47.php


# Removed File Name

# Usage: python3 convertStep2bTXTtshark_tcp_udp_csv.py <tshark_conv_file> [normal]

import re
import sys
import os.path

CSV_HEADER = 'traffic_type,protocol,address_a,port_a,address_b,port_b,packets_b_to_a,bytes_b_to_a,packets_a_to_b,bytes_a_to_b,packets,bytes,relative_start,duration'

tfn = sys.argv[1] if len(sys.argv) > 1 else ''

tf = open(tfn, 'r')

lines = tf.readlines()

traffic_type = 'normal' if len(sys.argv) > 2 else 'ransomware'

protocol = (lines[1][:3]).lower()

cfn = tfn.replace('.txt','.csv')
cf = open(cfn, 'w')

cf.write(CSV_HEADER+'\n')
for line in lines [5:-1]: #Ignore the first 4 lines and the last line
    fields = [traffic_type, protocol] + re.split(r'\s+<->\s|:|\s+',line.rstrip())
    
    #Optional. Add any field processing code here
    
    csvline = ','.join(fields) #+ ',' + os.path.basename(tfn).split('.')[0] #File name without suffixes
    cf.write(csvline+'\n')

tf.close()
cf.close()
