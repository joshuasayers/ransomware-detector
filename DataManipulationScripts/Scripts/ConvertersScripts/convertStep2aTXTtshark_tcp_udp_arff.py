# Convert tshark conversation output into arff
# Mostafa Dahshan <https://github.com/mdahshan>

# References
# https://www.w3resource.com/python-exercises/re/python-re-exercise-47.php
# https://waikato.github.io/weka-wiki/formats_and_processing/arff_stable/

# FIle Name not Included

# Usage: python3 convertStep2TXTtshark_tcp_udp_arff <tshark_conv_file> [normal]

import re
import sys
import os.path
import ipaddress

ARFF_HEADER = '''@relation network_conversations

@attribute traffic_type {ransomware,normal}
@attribute protocol {tcp,udp}
@attribute address_a numeric
@attribute port_a numeric
@attribute address_b numeric
@attribute port_b numeric
@attribute packets_b_to_a numeric
@attribute bytes_b_to_a numeric
@attribute packets_a_to_b numeric
@attribute bytes_a_to_b numeric
@attribute packets numeric
@attribute bytes numeric
@attribute relative_start numeric
@attribute duration numeric

@data
'''

tfn = sys.argv[1] if len(sys.argv) > 1 else ''

tf = open(tfn, 'r')

lines = tf.readlines()

traffic_type = 'normal' if len(sys.argv) > 2 else 'ransomware'

protocol = (lines[1][:3]).lower()

cfn = tfn.replace('.txt','.arff')
cf = open(cfn, 'w')

cf.write(ARFF_HEADER)
for line in lines [5:-1]: #Ignore the first 4 lines and the last line
    fields = [traffic_type, protocol] + re.split(r'\s+<->\s|:|\s+',line.rstrip())
    
    #Optional. Add any field processing code here
    fields[2] = str(int(ipaddress.ip_address(fields[2]))) #Replace IP address with its numerical value
    fields[4] = str(int(ipaddress.ip_address(fields[4]))) #Replace IP address with its numerical value

    arffline = ','.join(fields) #+ ',' + os.path.basename(tfn).split('.')[0] #File name without suffixes
    cf.write(arffline+'\n')

tf.close()
cf.close()