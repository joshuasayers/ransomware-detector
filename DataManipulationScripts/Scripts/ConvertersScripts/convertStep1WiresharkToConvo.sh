#!/bin/sh
# Extract TCP and UDP conversations from .pcap file
# Mostafa Dahshan <https://github.com/mdahshan>
# Usage: 
# ./nameOfThisScript.sh /path/to/folder/containingPCAPs /destination/folderName
# Output headers of the files will be : traffic_type,protocol,port_a,port_b,packets_b_to_a,bytes_b_to_a,packets_a_to_b,bytes_a_to_b,packets,bytes,relative_start,duration

PCAP_DIR=$1
TXT_DIR=$2

echo $PCAP_DIR
echo $TXT_DIR

for pcap in "$PCAP_DIR"/*.pcap
do
  tshark -r $pcap -q -z conv,tcp > "$TXT_DIR"/$(basename "${pcap%.*}")-tcp.txt
  tshark -r $pcap -q -z conv,udp > "$TXT_DIR"/$(basename "${pcap%.*}")-udp.txt
done

