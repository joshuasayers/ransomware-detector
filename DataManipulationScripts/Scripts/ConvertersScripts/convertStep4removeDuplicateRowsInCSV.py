# Requires python3 for pandas to work
# python3 convertStep4removeDuplicateRowsInCSV /path/to/file.csv

import pandas as pd
import sys

file_name = sys.argv[1]
file_name_output = "file_without_duplicate_rows.csv"

df = pd.read_csv(file_name, sep=",")

# Notes:
# - the `subset=None` means that every column is used 
#    to determine if two rows are different; to change that specify
#    the columns as an array
# - the `inplace=True` means that the data structure is changed and
#   the duplicate rows are gone  
df.drop_duplicates(subset=None, inplace=True)

# Write the results to a different file
df.to_csv(file_name_output, index=False)