# Merge all csv files in folder using Pandas library
# Pandas Reference: https://pandas.pydata.org/pandas-docs/stable/reference/io.html
# python3 3.8.2
#python3 convertStep3csvMerger "/path/to/folder/that/contains/csvs"
# Will skip erroneous lines in a file!

import os
import sys
import pandas as PandasLibrary

def mergeCSVs():
    #Output file name:
    csvOut = 'consolidated.csv'
    #Path to scan for CSVs
    csvDir = sys.argv[1]

    #Check if directory exists:
    if os.path.isdir(csvDir):
        #Gather all CSVs in the directory
        dir_tree = os.walk(csvDir)
        #Declare and load the variables for the filenames in dir_tree
        for dirpath, dirnames, filenames in dir_tree:
            pass

        #Prepare list of dataframes (pandas library uses dataframes as data structure to store tables)
        csvListDataFrames = []
        
        #Iterate through each filename
        for file in filenames:
            #Generate a full path to file
            fullFileName = csvDir + str(file)
            #Only if it is a file of extension csv process it
            if fullFileName.endswith('.csv'):
                #Log the file name
                print(file)
                # Read the csv into a data frame data structure
                dataFrame = PandasLibrary.read_csv(fullFileName, index_col=None, error_bad_lines=False, header=0)
                # Append it to list
                csvListDataFrames.append(dataFrame)
        
        #Create a dataframe to store the merged frames (made from array csvListDataFrames)
        mergedFrames = PandasLibrary.concat(csvListDataFrames, axis=0, sort=False, ignore_index=True)
        #Export dataframes to csv - remove indexes added by Pandas
        mergedFrames.to_csv(csvDir + csvOut, sep=',', encoding='utf-8', index=False)
    else:
        # It is not a directory do not proceed
        print('Invalid directory provided.')
        exit()

mergeCSVs()