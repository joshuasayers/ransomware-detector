import re

# Testing of Script, here is the executing signature:
# ./nameOfThisScript.sh /path/to/folder/containingPCAPs /destination/folderName


filePathToTestFolderUnconverted = "/home/blackdwarf/Desktop/RansomwareDetectorProject/GitLabRepository/ransomware-detector" \
                                  "/DataManipulationScripts/Scripts/ConvertersScripts/ConverterTests" \
                                  "/UnconvertedTestFiles "

filePathToTestFolderConverted = "/home/blackdwarf/Desktop/RansomwareDetectorProject/GitLabRepository/ransomware" \
                                "-detector" \
                                "/DataManipulationScripts/Scripts/ConvertersScripts/ConverterTests/ConvertedTestFiles"


# Test whether the normal pcap file is succesfully changed into a tcp and udp txt file.
def testingConvertStep1WiresharkToConvo(bash):
    output = bash.send(
        "../convertStep1WiresharkToConvo.sh " + filePathToTestFolderUnconverted + " " + filePathToTestFolderConverted)
    result = bash.path_exists("./ConvertedTestFiles/normal-tcp.txt") & bash.path_exists(
        "./ConvertedTestFiles/normal-udp.txt")
    bash.send_raw("rm -rf ./ConvertedTestFiles/normal-tcp.txt ./ConvertedTestFiles/normal-udp.txt")
    assert result


# Test whether the ransomware pcap file is succesfully changed into a tcp and udp txt file.
def testingConvertStep1WiresharkToConvoRansomware(bash):
    output = bash.send(
        "../convertStep1WiresharkToConvo.sh " + filePathToTestFolderUnconverted + " " + filePathToTestFolderConverted)
    result = bash.path_exists("./ConvertedTestFiles/ransomware-tcp.txt") & bash.path_exists(
        "./ConvertedTestFiles/ransomware-udp.txt")
    bash.send_raw("rm -rf ./ConvertedTestFiles/ransomware-tcp.txt ./ConvertedTestFiles/ransomware-udp.txt")
    assert result
