# Questions

After watching the video to gain an understanding of the work and progress acheived by the Ransomware Detector team, 
please answer the following questions with regards to your satisfaction in the progress of the team.

Please rate how satisfied you are with the following deliverables, 
(1 being strongly disagree, 5 being neutral and 10 being strongly agree):

- You are satisfied that the Ransomware Team have created and trained an initial machine learning model for 
detecting ransomware from network traffic packets.

1 2 3 4 5 6 7 8 9 10

- You are satisfied that the machine learning model is compatible with the Machine Learning environment that you use.

1 2 3 4 5 6 7 8 9 10

- You are satisfied that the Ransomware Detector team has used the appropriate development environment that follows
and compliments your research.

1 2 3 4 5 6 7 8 9 10

- It would be easy for you to import, train and using our machine learning model for your research.

1 2 3 4 5 6 7 8 9 10

- The Ransomware Detector team has made available to you the documentation that was created as per ITC303 requirements.

1 2 3 4 5 6 7 8 9 10

- The Ransomware Detector team have gathered the required Normal Data that you may use in your research.

1 2 3 4 5 6 7 8 9 10

- Your research was considered and implemented in the project.

1 2 3 4 5 6 7 8 9 10

- Any feedback or ideas that you have provided to the Ransomware Team were considered and implemented in the project.

1 2 3 4 5 6 7 8 9 10


## Questions

- Are you satisfied with the progress made by the ransomware detector team?
Yes, if No Why?

- Has the team demonstrated an understanding of the basic concepts required to deliver the project to completion in the future?
Yes, if No Why? 

- Based on the current progress, how confident are you that the model, data gathering and machine learning efforts may complement your research?
Yes, if No Why? 

- Are you satisfied that the team has gathered substantial amount of Normal Data that may benefit your research?
Yes, if No Why? 


## Overall Satisfaction
Considering that project is still halfway through the project plan timeline.
Please rate the following question from 1 to 10 (1 being strongly disagree, 5 being neutral and 10 being strongly agree).

Up until this current time, the Ransomware Detector team has met all your requirements for this project.
1 2 3 4 5 6 7 8 9 10



Thank you for your participation and all contributions towards this project.
