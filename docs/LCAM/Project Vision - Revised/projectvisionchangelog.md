# Project Vision Change Log

Please view change log image to understand the following line references. Line references correspond to the old document.

Line 31:
Line 31 paragraph was changed to better embrace the scope and deliverable agreed upon by sponsors and the team after multiple meetings.
Introducing the idea that the identification of ransomware is performed by looking at network traffic packets.

Line 32:
Wording changes to make text easier to read.

Line 34:
Line 34 corresponds to line 40 on the new project vision. Simply fixed the abbreviation notation and wording.

Line 35:
Fixed line 35 in line 41 on the new document to better correspond with the machine learning model that the team have agreed to build with the sponsors.
It is not merely a neural network but instead its a machine learning classifier model.
Minor wording fix.

Line 64:
Minor grammar change.

Line 104:
Heading

Line 110:
Updated the titles and names including roles.

Line 113:
INtroduced the Machine Learning Environment.

Line 114:
Introduced the machine learning model creation process.


The rest of the document complies with what our meetings have concluded.
