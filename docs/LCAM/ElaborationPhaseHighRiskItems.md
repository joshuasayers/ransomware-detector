Summary of Elaboration Phase High Risk Items performed and successfully deployed:
- 1 - Collective efforts towards gathering of Normal Data and uploading to a shared repository.
- 2 - Cleaning the data and applying basic feature extraction to match the same process performed by Mostafa to Ransomware Packet capture data.
    - It involves:
        - the conversion of PCAP files to TXT file conversations (5-tuple).
        - The conversion of TXT network flow conversation files to CSV.
        - Conversion of CSV files to ARFF for the Machine Learning environment to process.
- 3 - Proof that the data can be utilised for machine learning and it is compatible with Mostafa’s data.
- 4 - Achieved a user friendly way of creating, saving and sharing a model.
- 5 - Achieved a user friendly way of viewing reports of the test data and learning process.
