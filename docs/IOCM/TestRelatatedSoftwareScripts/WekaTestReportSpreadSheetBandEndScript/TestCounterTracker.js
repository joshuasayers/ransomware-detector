function resetTestCount() {
  PropertiesService.getScriptProperties().setProperty("testCount", "15");
}

function getTestCounter(){
 return parseInt(PropertiesService.getScriptProperties().getProperty("testCount")); 
}

function incrementTestCounter(){
 PropertiesService.getScriptProperties().setProperty("testCount", (getTestCounter() + 1).toString());
}
