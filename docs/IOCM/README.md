## Introduction To IOCM

__In this document 'model' refers to the exemplary process or procedure, where as 'ml model' refers to the software being produced.__


# Testing Model Process


- The testing of the machine learning models (ML Models) by the Ransomware Detector Team have been automated and programmed through Weka.

- The Ransomware Detector team has agreed on a set of evaluation (testing) criteria which must be performed at the end of every training session. The testing evaluates the model and generates statistics for evaluation criteria that will help the Ransomware Detector Team to find a candidate ML model to implement in the Hybrid Machine learning model.

- The Ransomware Detector team has carefully discussed and filtered the statistics required to measure the efficiency of the machine learning model against the sponsors' requirements.

- The Ransomware Detector Team has created a smart script powered google sheet to consolidate the test results and rank the models by accuracy.

- The Hybrid machine learning model is made up of the best 3 models that were created from the Model Construction Phases by Josh, Matt and Danny. Each team member will present their candidate model to be part of the Hybrid machine learning model.

Here is an example Hybrid Machine Learning model prediction process. In this example, there are 5 model classifiers which will take the dataset and output a prediction. The Final prediction is an average prediction obtained from all the models, which will impact the certainty of the prediction.


- Each created candidate model was tested 10 times through a 9th of the input molecules (molecules is WEKA ML framework terminology for instances or in Ransomware Detector Team terminology: fully completed network traffic packet conversations). For example, tested instances will be as high as: 275 557 number of tests through the model's evaluation.

- In the case of any defects were found during the creation of the model, e.g. dirty data or incompatible fields with models causing errors, the team has implemented a process for recording issues.


# Beta ML Model

The process to generating and beta testing our machine learning model is the beta testing model (model referred to here as in exemplary process, not as in machine learning model).

The beta ML Model that will be the combination of the best model from Josh, Matt and Danny as indicated by the testing performed.

The beta ML model is considered to be one that is:

Feature complete. - Must meet sponsor requirements and be able to classify Ransomware and Safe/Normal traffic packets. It must be compatible with the system that sponsors use.

No known bugs. - The ML must be tested as per the Standard evaluation testing process at the end of every ML Model training.

High quality code (ML Model) - The model must be ranked and the 3 highest ranking models must be the candidate models for the Hybrid machine learning model.




# Test Report Recording, Tracking and Auditing


__Well measured, well-presented, insightful, and objective evaluation of project progress against the specific aims of construction phase.__

The Ransomware Detector team believes that the metrics and statistics of the performance of the models during testing provide efficient evaluation of the progress of the project in terms of delivering a working model on each construction phase by each team member.

The Ransomware Detector team achieved insightful information by analyzing the model evaluation results and providing a report recording process that allows anyone that reviews the testing to trace the test results back to the key factors that contributed to the result.

For example, with the Summary reports provided by the Ransomware Team for each test, the reviewer is able to trace issues behind an abnormally high confusion matrix, such as an incorrect use of algorithm input or a dirty data molecule (instance, or internet traffic conversation in Ransomware Detector Team terms).

Discussion of all risks and issues encountered during the Construction Phase is extensive with an objective report produced on the status of those risks and issues, presented in a logical, easily accessed and understood manner.

The Ransomware Detector Team has implemented a few methods to meet this criteria:

- Iteration Plan Reviews include an open assessment of risks, issues, concerns, thoughts encountered during the construction phases and iterations as well as the progress acheived in construction.

- The Weka Test Results sheet contains an assessment report summarizing the model, the rationale for the choice of algorithm, the theory for the choice of attribute manipulation of the data including potential risks, flaws, and a report of the how the model was constructed.

Evaluation and reporting on specific progress achieved in each Construction iteration is comprehensive with detailed evidence of continuous improvement.

- To demonstrate continuous improvement the Ransomware Detector Team are constantly tracking the accuracy of the models throughout the training process and other important metrics for algorithm specific requirements.

- The Ransomware Detector Team has evaluated all created models and recorded the reports generated and analyzed the results for each Construction iteration.


Version Control for the construction phase is compounded by a combination of Git and Weka Test Results smart sheet (Google Version History).




Google Sheet provides both the ability to log and automate reports using the Scripting feature of google apps script. The Google Apps Script framework allows for maximum automation with minimum code, whereas the equivalent functionality on a database or folder structure would take greater development and testing of the automation of machine learning model result ranking and management. Google Sheet's version history will provide traceability required in cases where git cannot be used.

Sheet URL

https://docs.google.com/spreadsheets/d/1Xz4mG4qqhNVzBFx3BWzqvBm5mJn8t_QXcboPjfUdHYE/edit#gid=350205175on



# Project Managment during Construction Phase

__How well does the project status demonstrate the ability to evaluate the project process and outcomes, including team management, against the project proposal?__

The Ransomware Detector team has received consistent praise for the project's progress, team collaboration and the project management contribution demonstrated by each team member. The Ransomware Detector team believes that the project status assessment demonstrates the ability to evaluate the project, with the consideration of the feedback and input from project supervisors (David Tien) and Sponsors.

# Testing Manual

__How well does the user manual demonstrate technical writing skills in a software development environment? Students produce a high quality comprehensive user manual that effectively targets the intended audience both in terms of the amount of information presented and the presentation style including detailed illustrations.__

The Ransomware Detector Team has put together a series of illustrated documents that make up the Testing Manual. This collection of illustrated documents instruct the target user on how to evaluate the candidate machine learning models by:

- Stepping the user through walking through setup

- Stepping the user through configuration of the environment (evaluation settings)

- Stepping the user through evaluation of the machine learning model by the process of classifying data and obtaining an evaluation test result report (which includes metrics and key performance indicators of the candidate model).

The target readers of the manual were identified as the project sponsors (whom are proficient with using Weka) and thus the language and steps used are thorough but utilize the common knowledge amongst the team and the sponsors to explain steps.
