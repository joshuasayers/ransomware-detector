function createNewSheet(){
  let currentSpreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  let exampleSheet = currentSpreadsheet.getSheetByName(TEMPLATESHEETNAME);
  let copiedSheet = exampleSheet.copyTo(currentSpreadsheet);
  let proposedSheetName = TESTSHEETPREFIXNAME + getTestCounter();
  copiedSheet.setName(proposedSheetName);
  populateTheTestNameInItsSummaryTable(copiedSheet, proposedSheetName);
  incrementTestCounter();
}
  



/**
   Populates the test name on the summary table
*/
function populateTheTestNameInItsSummaryTable(sheet, testName){
  let candidateRow = [];
  let dataRangeValues = sheet.getDataRange().getValues();
  for(let i = 0; i < dataRangeValues.length; i++){
    let currentRow = dataRangeValues[i];
    let colindex = currentRow.indexOf(SUMMARYHEADERS[0]);
    let isTheDataRowHeaders = true;  
    //If it found something check the next value
    if(colindex > -1){
      let rowIndexToTest = colindex;
      for(let headerIndex in SUMMARYHEADERS){
        if(currentRow[rowIndexToTest].trim() !== SUMMARYHEADERS[headerIndex].trim()){
          isTheDataRowHeaders = false;
        }
        rowIndexToTest += 1;
      }
    }
    
    if(colindex > -1 && isTheDataRowHeaders){
      //Write to field under Header for test name: (indexs for column and row are 1 based not zero based)
      sheet.getRange(i + 2, colindex + 1).setValue(testName);
    }
  }
}
