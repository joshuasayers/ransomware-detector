function copySummaryTableRowToDataSummaryPage(){
  let summarySheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(SUMMARYSHEETNAME);
  let twoDimArrayTable = get2DArrayToPopulateSummaryTableDataSheet();
  console.log(twoDimArrayTable);
  summarySheet.clear();
  summarySheet.getRange(1,1,twoDimArrayTable.length,twoDimArrayTable[0].length).setValues(twoDimArrayTable);
}


function get2DArrayToPopulateSummaryTableDataSheet(){
  let arrayOfSummaryTableJSONRows = []
  //Fill in header:
  arrayOfSummaryTableJSONRows.push(SUMMARYHEADERS);
  
  //Get All Sheets that start prefixed "Test"
  let arrayOfTestSheets = getAllTestSheets();
  
  for(let sheetIndex in arrayOfTestSheets){
    let currentTestSheet = arrayOfTestSheets[sheetIndex];
  //Get Rows of data as an Array of JSON object
    let arrayOfCells = getDataFromItsSummaryTable(currentTestSheet);
    arrayOfSummaryTableJSONRows.push(arrayOfCells);
  }
  //Write JSON Object to Summary Main Table overwriting
  return arrayOfSummaryTableJSONRows; 
}



/**
Get all the test sheets that startWith the TESTSHEETPREFIXNAME
*/
function getAllTestSheets(){
  let arrayOfTestSheets = [];
  let arrayOfSheets = SpreadsheetApp.getActiveSpreadsheet().getSheets();
  for(let sheetIndex in arrayOfSheets){
    let sheet = arrayOfSheets[sheetIndex];
    if(sheet.getName().toString().startsWith(TESTSHEETPREFIXNAME)){
      console.log(sheet.getSheetName());
      arrayOfTestSheets.push(sheet.getSheetName());
    }
  }
  return arrayOfTestSheets;
}



/**
   Uses the SUMMARYHEADERS global variables to determine which row contains the data to extract from the sheet by name sheetName
   returns array of values in order of SUMMARYHEADERS
*/
function getDataFromItsSummaryTable(sheetName){
  let candidateRow = [];
  console.log(sheetName);
  let dataRangeValues = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(sheetName).getDataRange().getValues();
  for(let i = 0; i < dataRangeValues.length; i++){
    let currentRow = dataRangeValues[i];
    let colindex = currentRow.indexOf(SUMMARYHEADERS[0]);
    let isTheDataRowHeaders = true;  
    //If it found something check the next value
    if(colindex > -1){
      let rowIndexToTest = colindex;
      for(let headerIndex in SUMMARYHEADERS){
        if(currentRow[rowIndexToTest].trim() !== SUMMARYHEADERS[headerIndex].trim()){
          isTheDataRowHeaders = false;
        }
        rowIndexToTest += 1;
      }
    }
    
    if(colindex > -1 && isTheDataRowHeaders){
      candidateRow = dataRangeValues[i + 1].slice(colindex);
      console.log(candidateRow);
      return candidateRow;
    }
  }
}
