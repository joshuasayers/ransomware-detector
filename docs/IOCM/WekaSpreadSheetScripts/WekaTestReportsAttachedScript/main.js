//Global Vars
let TEMPLATESHEETNAME = "Template";
let TESTSHEETPREFIXNAME = "Test"; //number will get appended
let SUMMARYHEADERS = ["Test #", "Classifier Name", "Correctly Classified Instances #", "Correctly Classified Instances %", "Incorrectly Classified Instances #", "Incorrectly Classified Instances %", "Total Number of Instances"];
let SUMMARYSHEETNAME = "All Tests Data Summary"






function onOpen(e) {
  //Add buttons to the gui.
  SpreadsheetApp.getUi().createMenu('Ransomware').addItem('Create new Test sheet', 'createNewSheet').addSeparator().addItem("Sync Main Summary Table", 'copySummaryTableRowToDataSummaryPage').addToUi();
}


/*
Unlike Node javascript, google app script will read from any .gs file without importing.
So functions are only separated into files for clarity of logical thinking.

onOpen is a built-in overriden function that runs whenever the webpage of the spreadsheet reloads.
*/